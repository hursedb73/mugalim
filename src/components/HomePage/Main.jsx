import React, { useRef, useEffect, useState } from 'react';
import './index.scss';  
import Form from './Form/Form' 

import Vector111 from '../../assets/Vector111.png';
import Vector88 from '../../assets/Vector88.png';
import Vector7Ball from '../../assets/Vector7Ball.png';
import GroupLamp71 from '../../assets/GroupLamp71.png';
import Group23 from '../../assets/Group23.png';
import Group33 from '../../assets/Group33.png';
import Group32 from '../../assets/Group32.png';
import Group20 from '../../assets/Group20.png';
import Group40 from '../../assets/Group40.png';
import Group45 from '../../assets/Group45.png';
import Group47PK from '../../assets/Group47PK.png';
import Group48 from '../../assets/Group48.png';
import GroupBook64 from '../../assets/GroupBook64.png';
import Group88Mob from '../../assets/Group88Mob.png';
import Group89Mess from '../../assets/Group89Mess.png';
import Group58 from '../../assets/Group58.png';
import Group43 from '../../assets/Group43.png';
import Group96 from '../../assets/Group96.png'; 
import group67Earth from '../../assets/Group67Earth.png'; 
import Group68A from '../../assets/Group68A.png'; 
import Star from '../../assets/star.png';
import Asset11 from '../../assets/Asset11.png';
import Frame from '../../assets/Frame.png';

const Main = () => {

  const formRef = useRef(null); 

  const [scrollOffset, setScrollOffset] = useState(0);  

  const updateScrollOffset = () => {
    const windowWidth = window.innerWidth;
    if (windowWidth <= 768) {
      setScrollOffset(4145);
    } else if (windowWidth <= 1200) {
      setScrollOffset(2450);
    } else {
      setScrollOffset(2850);
    }
  };

  const handleClick = () => {
    const targetFormRef = formRef.current;
    if (targetFormRef) {
      const { top } = targetFormRef.getBoundingClientRect();
      window.scrollTo({
        top: top + scrollOffset,
        behavior: 'smooth',
      });
    }
  };

  useEffect(() => {
    updateScrollOffset(); // Call the function once during initial render
    window.addEventListener('resize', updateScrollOffset);
    return () => {
      window.removeEventListener('resize', updateScrollOffset);
    };
  }, []);
 
  return (
    <div>
      <main>
        <div>
          <div className='block-content'>
            <div className='block-container'>
              <img className='vectorBulb' src={Group40} alt="Loading..." />
              <img className='vectorBulb2' src={Group40} alt="Loading..." />
              <img className='vectorPK' src={Group47PK} alt="Loading..." />
              <img className='vectorEarth' src={Group48} alt="Loading..." />
              <img className='vectorMob' src={Group88Mob} alt="Loading..." />
              <img className='vectorMob2' src={Group88Mob} alt="Loading..." />
              <img className='vectorpencil' src={Group58} alt="Loading..." />
              <img className='vectorPencil' src={Group43} alt="Loading..." />
              <img className='vectorPencil23' src={Group23} alt="Loading..." />
              <img className='academicСap' src={Group33} alt="Loading..." />
              <img className='bamboo' src={Group45} alt="Loading..." />
              <img className='bamboo2' src={Group45} alt="Loading..." />
              <img className='ball' src={Vector111} alt="Loading..." /> 
              <img className='oneSt' src={Group20} alt="Loading..." />
              <img className='oneSt2' src={Group20} alt="Loading..." />
              <img className='group89Mess' src={Group89Mess} alt="Loading..." />
              <img className='star' src={Star} alt="Loading..." />
              <img className='star1' src={Star} alt="Loading..." />
              <img className='star2' src={Star} alt="Loading..." />
              <img className='star3' src={Star} alt="Loading..." />
              <img className='star4' src={Star} alt="Loading..." />
              <img className='star5' src={Star} alt="Loading..." />
              <img className='star6' src={Star} alt="Loading..." />
              <img className='star7' src={Star} alt="Loading..." />
              <img className='star8' src={Star} alt="Loading..." />
              <img className='star9' src={Star} alt="Loading..." />
              <img className='star93' src={Star} alt="Loading..." />
              <div className='container'>
                <img className='ball2' src={Vector7Ball} alt="Loading..."/>
                <img className='group68A' src={Group68A} alt="Loading..."/> 
                <img className='group67Earth' src={group67Earth} alt="Loading..."/>
                <img className='group89Mess2' src={Group89Mess} alt="Loading..." />
                <img className='oneSt3' src={Group20} alt="Loading..." />
                <img className='group68A2' src={Group68A} alt="Loading..."/> 
                <img className='vector8Lamp' src={GroupLamp71} alt="Loading..."/> 
                <img className='GroupBook64' src={GroupBook64} alt="Loading..."/> 
                <h3 className='idealPlan'>Идеальный план !</h3>
                <img className='group67Earth2' src={group67Earth} alt="Loading..."/>
                <p className="descriptionPlan">Дорогой Учитель, Педагог, Репетитор, Тренер
                  закажите себе "Идеальный план урока" по любому предмету и для любого класса.
                  <br />
                  Упростите себе задачу и сберегите время на более важные вещи.
                </p>  
                <button className='blockContentBtn' onClick={handleClick} >
                <p className='orderPlan'>Заказать план</p>
                </button> 
              </div>
              <img className='content-arrow' src={Group32} alt="Loading..." />
              <div className='block-case-study'>
                <div className='block1-case-study'>
                  <h4>ВНИМАНИЕ !</h4>
                  <p>Вы получите не просто план урока с активными методами обучения, а еще дополнительно к нему тестовые вопросы для формативного оценивания и задание для закрепления темы.</p>
                </div>
                <div className='block2-case-study'>
                  <img src={Asset11} alt="Loading..." />
                </div>
              </div>
              <div className='block-content-info'>
                <div className='block-content-image'>
                  <img className='content-frame' src={Frame} alt="Loading..." />
                </div> 
                  <h3 className='block-content-text1'>Как заказать разработку плана урока?</h3>
                  <h3 className='block-content-text2'>Всё очень просто !</h3>
                  <img className='block-content-textImg' src={Vector88} alt="loading..." /> 
                  <button className='block-content-btn'onClick={handleClick} >
                  <p className='block-content-text-btn'>Заказать план</p> 
                  </button> 
              </div>
              <img className='content-arrow2' src={Group96} alt="Loading..." /> 
                <div className='block-content-advertising-form1'>
                    <div className='block-content-advertising-form-block1'>
                      <div className='block-content-advertising-form-block2'>
                        <div className='block-content-advertising-form-block3'>
                          <div className='block-content-advertising-form-block4'>
                            <h4 className='block-content-advertising-form-block4-h4'>Заполните форму ниже</h4>
                          </div>
                          <div className='block-content-advertising-form-block5'>
                            <h4 className='block-content-advertising-form-block5-h4'>
                              1
                            </h4>
                          </div>
                        </div>
                      </div>
                    </div> 
                    <div className='block-content-advertising-form-block11'>
                      <div className='block-content-advertising-form-block2'>
                        <div className='block-content-advertising-form-block3'>
                          <div className='block-content-advertising-form-block4'>
                            <h4 className='block-content-advertising-form-block4-h4'>Укажите куда вы хотите получить план урока</h4>
                          </div>
                          <div className='block-content-advertising-form-block5'>
                            <h4 className='block-content-advertising-form-block5-h4'>
                              2
                            </h4>
                          </div>
                        </div>
                      </div>
                    </div>   
                    <div className='block-content-advertising-form-block22'>
                      <div className='block-content-advertising-form-block2'>
                        <div className='block-content-advertising-form-block3'>
                          <div className='block-content-advertising-form-block4'>
                            <h4 className='block-content-advertising-form-block4-h4'>Получите свой идеальный план урока с тестами и заданиями к нему</h4>
                          </div>
                          <div className='block-content-advertising-form-block5'>
                            <h4 className='block-content-advertising-form-block5-h4'>
                              3
                            </h4>
                          </div>
                        </div>
                      </div>
                    </div>   
              </div>
            </div>
          </div>
        </div >  
        <div ref={formRef} id='formSection' style={{ display: 'none' }} >
          <Form />
        </div>
      </main>  
    </div>
  );
}

export default Main;