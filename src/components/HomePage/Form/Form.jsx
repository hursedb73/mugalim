import './index.scss';
import { useState } from 'react';
import { forwardRef } from 'react';
import Group72 from '../../../assets/Group72.png'; 
import { db } from '../../../firebase.js';
import { addDoc, collection } from 'firebase/firestore'; 

const Form = ({id}) => {

  const [name, setName] = useState('');
  const [item, setItem] = useState('');
  const [classroom, setClassroom] = useState('');
  const [subject, setSubject] = useState('');
  const [lesson, setLesson] = useState(''); 
  const [email, setEmail] = useState('');
  const [tel, setTel] = useState('');
  const [errors, setErrors] = useState({
    name: '',
    item: '',
    classroom: '',
    subject: '',
    lesson: '', 
    email: '',
    tel: '',
  });

  const nameHandler = (e) => {
    setName(e.target.value);
  };

  const itemHandler = (e) => {
    setItem(e.target.value);
  };

  const classroomHandler = (e) => {
    setClassroom(e.target.value);
  };

  const subjectHandler = (e) => {
    setSubject(e.target.value);
  };
  
  const lessonHandler = (e) => {
    setLesson(e.target.value);
  };

  const emailHandler = (e) => {
    setEmail(e.target.value);
  };

  const telHandler = (e) => {
    setTel(e.target.value);
  };
 

  const submitHandler = async (e) => {
    e.preventDefault();
    let isValid = true;
    const newErrors = {};

    if (!name) {
      newErrors.name = 'Пожалуйста, введите "ФИО"';
      isValid = false;
    }

    if (!item) {
      newErrors.item = 'Пожалуйста, введите "Предмет"';
      isValid = false;
    }

    if (!classroom) {
      newErrors.classroom = 'Пожалуйста, введите "Класс"';
      isValid = false;
    }

    if (!subject) {
      newErrors.subject = 'Пожалуйста, введите "Тему"';
      isValid = false;
    }
    if (!lesson) {
      newErrors.lesson = 'Пожалуйста, введите "Цель урока"';
      isValid = false;
    } 
    if (!email) {
      newErrors.email = 'Пожалуйста, введите "Email"';
      isValid = false;
    }
    if (!tel) {
      newErrors.tel = 'Пожалуйста, введите "Телефон"';
      isValid = false;
    }

    if (isValid) {
      await addDoc(collection(db, 'Users'), {
        name: name,
        item: item,
        classroom: classroom,
        subject: subject,
        lesson: lesson, 
        email: email,
        tel: tel,
      });
      setName('');
      setItem('');
      setClassroom('');
      setSubject('');
      setLesson(''); 
      setEmail('');
      setTel('');
      setErrors({});
    } else {
      setErrors(newErrors);
    }
  };

  return (
    <div className='container-form' id={id} >
      <div className='container-form-block1' >  
        <h3 className='container-form-block1-h3'>Заявка</h3>
        <div className='input-container'>
        <form className='form' onSubmit={submitHandler} >
        <input
              className='input-text'
              name='usename'
              value={name}
              onChange={nameHandler}
              type='text'
              placeholder='ФИО'
              />
              {errors.name && <p className='error'>{errors.name}</p>}
            <input
              className='input-text'
              name='item'
              value={item}
              onChange={itemHandler}
              type='text'
              placeholder='Предмет'
            />
            {errors.item && <p className='error'>{errors.item}</p>}
            <input
              className='input-text'
              name='class'
              value={classroom}
              onChange={classroomHandler}
              type='text'
              placeholder='Класс'
            />
            {errors.classroom && <p className='error'>{errors.classroom}</p>}
            <input
              className='input-text'
              name='subject'
              value={subject}
              onChange={subjectHandler}
              type='text'
              placeholder='Тема'
            />
            {errors.subject && <p className='error'>{errors.subject}</p>}
          <input 
            className='input-text' 
            name='lesson'
            value={lesson}
            onChange={lessonHandler}
            type='text' 
            placeholder='Цель урока' />
            {errors.lesson && <p className='error'>{errors.lesson}</p>} 
          <input 
            className='input-text' 
            name='email'
            value={email}
            onChange={emailHandler}
            type='email' 
            placeholder='Email' />
            {errors.email && <p className='error'>{errors.email}</p>}
          <input 
            className='input-text' 
            name='tel'
            value={tel}
            onChange={telHandler}
            type='tel' 
            placeholder='Телефон' /> 
            {errors.tel && <p className='error'>{errors.tel}</p>}
            <button className='block-content-btn' type='submit'>
            <p className='block-content-text-btn'>Заказать план</p>
            </button>
            </form>
        </div> 
      </div>
      <div className='container-form-block2'>
        <div className='text-box-lesson'>
           <p className='container-form-block2-p1'>Шаг за шагом к </p> 
           <p className='container-form-block2-p2'>идеальному уроку !</p>
        </div>
        <img className='group72' src={Group72} alt="Loading..." />
      </div>
    </div>
  );
}

export default forwardRef(Form);
