import React from 'react';
import './index.scss';
import Header from './Header'
import Main from './Main'; 
import Footer from './Footer/Footer';

const HomePage = () => {   
  return (
    <div>
      <Header  /> 
      <Main/>  
      <Footer/>
    </div>
  );
};

export default HomePage;  

 
