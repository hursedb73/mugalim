import React from 'react';
import './index.scss';
import VectorIcon from '../../../assets/VectorIcon.png';
import Isolation from '../../../assets/Isolation.png';
import Group36 from '../../../assets/Group36.png';
import Whatsapp from '../../../assets/whatsapp.png';
import Telegram from '../../../assets/telegram.png';
import Instagram from '../../../assets/instagram.png';

const Footer = () => {
  return ( 
    <footer className="footer">
      <div className="containerF">
        <div className="row">
          <div className="footer-col1">
            <img className="vectorIcon" src={VectorIcon} alt="Loading..." /> 
          </div>
          <div className="footer-col2">
            <h4 className='contactsFooter'>Контакты</h4> 
                 <a className='footer-link' href="https://wa.me/996700030199" target="_blank" rel="noreferrer">
                    <img className='phoneFooter' src={Isolation} alt="Loading..." />
                    <p className='phoneNumberFooter'>+(996)700-03-01-99</p>
                </a>
                <a className='footer-link' href="mailto:mugalim.edu@gmail.com" target="_blank" rel="noreferrer">
                    <img className='emailFooter' src={Group36} alt="Loading..." />
                    <p className='emailFooterP'>mugalim.edu@gmail.com</p>
                </a>
          </div>
          <div className="footerIcons">
            <h4>Соцсети</h4>
                <div className="social-links"> 
                    <a className='footer-link' href="https://wa.me/996700030199" target="blank" >
                        <img className='whatsappFooter' src={Whatsapp} alt="" />
                    </a>
                    <a className='footer-link' href="https://t.me/mugalim_edu" target="blank" >
                    <img className='telegramFooter' src={Telegram} alt="" />
                    </a>
                    <a className='footer-link' href="https://www.instagram.com/mugalim_edu/" target="blank" >
                    <img className='insatgramFooter' src={Instagram} alt="" />
                    </a>
                </div>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
