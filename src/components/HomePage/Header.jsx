import React, { useRef, useEffect, useState } from 'react';
import './index.scss'; 
import Form from './Form/Form';

import VectorIcon from '../../assets/VectorIcon.png';
import VectorBand from '../../assets/VectorBand.png';
import VectorBand2 from '../../assets/VectorBand2.png';
import Ellipse from '../../assets/Ellipse2.png';
import Model from '../../assets/Model.png';
import Vector9 from '../../assets/Vector9.png';
import Vector10 from '../../assets/Vector10.png';
import Vector11 from '../../assets/Vector11.png'; 
import Group131 from '../../assets/Group131.png';

const Header = () => {
  const formRef = useRef(null);
  const [scrollOffset, setScrollOffset] = useState(0);  

  const updateScrollOffset = () => {
    const windowWidth = window.innerWidth;
    if (windowWidth <= 768) {
      setScrollOffset(3450);
    } else if (windowWidth <= 1200) {
      setScrollOffset(2450);
    } else {
      setScrollOffset(2450);
    }
  };

  const handleClick = () => {
    const targetFormRef = formRef.current;
    if (targetFormRef) {
      const { top } = targetFormRef.getBoundingClientRect();
      window.scrollTo({
        top: top + scrollOffset,
        behavior: 'smooth',
      });
    }
  };

  useEffect(() => {
    updateScrollOffset();
    window.addEventListener('resize', updateScrollOffset);
    return () => {
      window.removeEventListener('resize', updateScrollOffset);
    };
  }, []);

  return (
    <div>
        <header className='headerHome'> 
            <div className='imageContainer'>
              <img className='vectorIconHeader' src={Vector9} alt='Loading...' />
              <div className='vectorIconBlock'>
                <img className='vector-icon-favicon' src={VectorIcon} alt="Loading..." />
              </div>
              <div className='vectorBandBlock'>
                <img src={VectorBand}  alt="Loading..." />
              </div>
            </div>
            <div className='imageContainer2'>
              <div className='vectorEllipse'>
                <img src={Ellipse} alt="Loading..." />
              </div>
                <a href="https://wa.me/996700030199" target="_blank" rel="noreferrer">
              <div className='vectorPhoneNumber'>
                 <img className="PhoneNumber" src={Group131} alt="Loading..." />
                 <p className='phone-number-call'>+(996)700-03-01-99</p>
              </div>
                </a>
              <div className='vectorBandBlock2'>
                <img src={VectorBand2} alt="Loading..." />
              </div>
            </div>
            <div className='vectorBandBlock3'>
              <div className='vectorModel'>
                <img className='vector-model' src={Model} alt="Loading..." />
              </div>
            </div>
            <div className='vectorBandBlock4'>
              <div className='vectorIconBlock'>
                <img src={Vector10} alt="Loading..." />
              </div>
              <div className='vectorEllipse2'>
                <img src={Vector11} alt="Loading..." />
              </div>
            </div>
            <div className='vectorBandText'>
              <div className='vectorEllipseText'>
                <p>ЗАКАЖИ ПЛАН УРОКА !  </p>
                <p className='paragraphPln'>И повысь успеваемость своих учеников</p>
              </div>
            </div> 
            <button className='vectorButton' onClick={handleClick}>
               <p>Заказать план</p>
            </button>
          <div ref={formRef} id='formSection'>
          <Form />
        </div>
     </header >     
  </div> 
  )
}

export default Header