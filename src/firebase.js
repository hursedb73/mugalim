import { initializeApp } from "firebase/app"; 
import { getFirestore } from "@firebase/firestore";  

const firebaseConfig = {
    apiKey: "AIzaSyB_lRguJYJWB0uwSUdNsdNNzjXINoQMNdk",
    authDomain: "mugalimproject-d8d63.firebaseapp.com",
    projectId: "mugalimproject-d8d63",
    storageBucket: "mugalimproject-d8d63.appspot.com",
    messagingSenderId: "444984188637",
    appId: "1:444984188637:web:ceff193fb3cdb474ce0534"
  } 
  
  const app = initializeApp(firebaseConfig);
  const db = getFirestore(app);
  
  export { db };